#!/bin/bash

echo "Enter a positive number "
read natnum

if [ $natnum -lt 0 ]
then 
   echo "Not a natural number"
   exit 1
fi
num=$natnum
sum=0
while [ $num -gt 0 ]
do 
   sum=$(($sum + $num))
   num=$((num - 1))
done
echo "Sum is: $sum"
