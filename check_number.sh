#!/bin/bash

echo "Enter a num "
read number

if [ ${number} -eq 0 ]
then
   echo "Number is zero"
elif [ ${number} -gt 0 ]
then
   echo "Number is positive"
else
   echo "Number is negative"
fi

