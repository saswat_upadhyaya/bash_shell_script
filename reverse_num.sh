#!/bin/bash

echo "enter number to be reversed"
read num

revnum=0
tmp=0
while [ $num -gt 0 ]
do 
  tmp=$(( $num % 10 ))
  revnum=$(( ( $revnum * 10 ) + $tmp ))
  num=$(( $num / 10 ))
done
echo "reversed number is $revnum"